function love.load()
  cel = 50
  screen_w = love.graphics.getWidth()
  screen_h = love.graphics.getHeight()
  cell_count_w = (screen_w/cel)-1
  cell_count_h = (screen_h/cel)-1
  snake = {{10,7}, {11,7}}
  food = {love.math.random(0,cell_count_w), love.math.random(0,cell_count_h)}
  direction = "left"
  timer = 0
  threshold = 0.2
end

function love.update(dt)
  if timer < threshold then
    timer = timer + dt
    return
  end
  timer = timer - threshold
  head = snake[1]

  if direction == "left" then
    newpos = {head[1]-1, head[2]}
  elseif direction == "up" then
    newpos = {head[1], head[2]-1}
  elseif direction == "right" then
    newpos = {head[1]+1, head[2]}
  elseif direction == "down" then
    newpos = {head[1], head[2]+1}
  end

  -- check if snake will collide with itself
  for _,snake_body in ipairs(snake) do
    if newpos[1] == snake_body[1] and newpos[2] == snake_body[2] then
      love.event.quit()
    end
  end

  table.insert(snake, 1, newpos)
  -- check if snake head is on food tile
  if newpos[1] == food[1] and newpos[2] == food[2] then
    food = {love.math.random(0,cell_count_w), love.math.random(0,cell_count_h)}
    print("eat")
  else
    table.remove(snake)
  end

  -- check if snake will exit visible field
  if newpos[1] < 0 or newpos[1] > cell_count_w then
    love.event.quit()
  end
  if newpos[2] < 0 or newpos[2] > cell_count_h then
    love.event.quit()
  end
end

function love.draw()
  -- draw field
  love.graphics.setColor(255,255,255,255)
  for x=0,cell_count_w do
    for y=0,cell_count_h do
      love.graphics.rectangle("fill",x * cel, y * cel,cel-1,cel-1)
    end
  end

  -- draw snake
  love.graphics.setColor(0,255,0,255)
  for _, pos in ipairs(snake) do
      love.graphics.rectangle("fill",pos[1] * cel, pos[2] * cel,cel-1,cel-1)
  end

  -- draw food
  love.graphics.setColor(0,155,0,155)
  love.graphics.rectangle("fill",food[1] * cel, food[2] * cel,cel-1,cel-1)
end

function love.keypressed(key,unicode)
  if key == "escape" then
    love.event.quit()
  end
  if key == "up" and direction ~= "down" then
    direction = "up"
  elseif key == "left" and direction ~= "right" then
    direction = "left"
  elseif key == "down" and direction ~= "up" then
    direction = "down"
  elseif key == "right" and direction ~= "left" then
    direction = "right"
  end
end
